package com.example.myapplication

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MainFragment : Fragment(R.layout.fragment_main), MovieRecyclerAdapter.ContentListener {

    private val database = Firebase.firestore
    private lateinit var recyclerAdapter: MovieRecyclerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_main, container, false)
        val searchInput = view.findViewById<EditText>(R.id.editTextSearch)




        val recyclerView = view.findViewById<RecyclerView>(R.id.MovieList)
        database.collection("movies")
            .get()
            .addOnSuccessListener { result ->
                val movieList = ArrayList<Movie>()
                for (data in result.documents) {
                    val movie = data.toObject(Movie::class.java)
                    if (movie != null) {
                        movie.id = data.id
                        movieList.add(movie)
                    }
                }
                recyclerAdapter = MovieRecyclerAdapter(movieList, this@MainFragment)
                recyclerView.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = recyclerAdapter
                }

                searchInput.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    }

                    override fun onTextChanged(query: CharSequence, p1: Int, p2: Int, p3: Int) {
                        query.toString().lowercase()
                        val filteredList = ArrayList<Movie>()
                        for (i in 0 until movieList.size) {
                            if (movieList[i].title.toString().lowercase().contains(query)) {
                                filteredList.add(movieList[i])
                            }
                        }
                        recyclerAdapter = MovieRecyclerAdapter(filteredList, this@MainFragment)
                        recyclerView.apply {
                            layoutManager = LinearLayoutManager(context)
                            adapter = recyclerAdapter
                        }
                    }

                    override fun afterTextChanged(p0: Editable?) {
                    }

                })
            }
            .addOnFailureListener { exception ->
                Log.w("MainFragment", "Error getting documents", exception)

            }
        return view
    }

    override fun onItemButtonClick(index: Int, movie: Movie, clickType: ItemClickType) {
        if (clickType == ItemClickType.EXPAND) {
            val bundle = Bundle()

            bundle.putString("imageUrl", movie.imageUrl)
            bundle.putString("title", movie.title)
            bundle.putString("description", movie.description)
            bundle.putString("year", movie.year)
            bundle.putString("rating",movie.rating)

            bundle.putString("backgroundUrl",movie.backgroundUrl)


            val movieViewFragment = MovieViewFragment()
            movieViewFragment.arguments = bundle

            val transaction = fragmentManager?.beginTransaction()
            transaction?.replace(R.id.activity, movieViewFragment)?.commit()


        }
    }
}