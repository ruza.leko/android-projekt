package com.example.myapplication

data class Movie(
    var id: String = "",
    var imageUrl: String? = null,
    var title: String? = null,
    var description: String? = null,
    var rating: String? = null,
    var year: String? = null,
    var backgroundUrl :String?=null
)
