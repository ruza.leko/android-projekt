package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

enum class ItemClickType {
    EXPAND
}

class MovieRecyclerAdapter(private val items: ArrayList<Movie>, val listener: ContentListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recycler_item, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is MovieViewHolder -> {
                holder.bind(position, items[position], listener)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class MovieViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        private val imageButton = view.findViewById<ImageView>(R.id.movieImage)
        private val title = view.findViewById<TextView>(R.id.titleTextView)



        fun bind(index: Int, movie: Movie, listener: ContentListener) {
            Glide.with(view.context).load(movie.imageUrl).into(imageButton)
            title.setText(movie.title)

            imageButton.setOnClickListener {
                listener.onItemButtonClick(index, movie, ItemClickType.EXPAND)
            }


        }
    }

    interface ContentListener {
        fun onItemButtonClick(index: Int, movie: Movie, clickType: ItemClickType)
    }
}