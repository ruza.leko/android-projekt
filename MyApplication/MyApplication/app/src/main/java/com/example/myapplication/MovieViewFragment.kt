package com.example.myapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase

class MovieViewFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.fragment_movie_view, container, false)
        val image = view.findViewById<ImageView>(R.id.movie_poster)
        val title = view.findViewById<TextView>(R.id.movie_name)
        val description = view.findViewById<TextView>(R.id.movie_overview)
        val year = view.findViewById<TextView>(R.id.movie_release_date)
        val rating = view.findViewById<TextView>(R.id.ratingTextView)
        val background=view.findViewById<ImageView>(R.id.movie_backdrop)

        val args = this.arguments

        title.text = args?.get("title").toString()
        description.text=args?.get("description").toString()
        year.text=args?.get("year").toString()
        rating.text=args?.get("rating").toString()
        Glide.with(view.context).load(args?.get("imageUrl").toString()).into(image)
        Glide.with(view.context).load(args?.get("backgroundUrl").toString()).into(background)

        val backButton = view.findViewById<ImageButton>(R.id.backButton)
            .setOnClickListener {
                val transaction = fragmentManager?.beginTransaction()
                transaction?.replace(R.id.activity, MainFragment())?.commit()
            }

        return view
    }
}